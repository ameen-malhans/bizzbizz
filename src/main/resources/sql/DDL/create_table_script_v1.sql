SET foreign_key_checks = 0;
DROP TABLE IF EXISTS member;


CREATE TABLE member (
id int(20) NOT NULL AUTO_INCREMENT,
firstName varchar(20) NOT NULL,
lastName varchar(255) DEFAULT NULL,
businessName varchar(255) DEFAULT NULL,
PRIMARY KEY (id)
);