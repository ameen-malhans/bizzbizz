// this is the id of the form
$("#contactUsForm").submit(function() {

    var url = addMessageUrl; // the script where you handle the form input.

    var formData = {
            'full_name'              : $("#full_name").val(),
            'email'             : $("#email").val(),
            'message'    : $("#message").val()
        };
    
    $.ajax({
           type: "POST",
           url: url,
           data: formData, // serializes the form's elements.
           success: function(data)
           {
               $("#messageSuccess").html(data); // show response from the php script.
           }
         });

    return false; // avoid to execute the actual submit of the form.
});

