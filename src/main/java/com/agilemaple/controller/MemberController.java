package com.agilemaple.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.agilemaple.constants.RestURIConstants;
import com.agilemaple.model.Member;
import com.agilemaple.service.MemberService;

@Controller
@RequestMapping("/api") //used to map web requests to Spring Controller methods
public class MemberController {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);

	@Autowired
	private MemberService memberService;

	@RequestMapping(value = RestURIConstants.GET_ALL_MEMBERS, method = RequestMethod.GET)
	public @ResponseBody List<Member> listMember() {
		logger.debug("Start listMember");
		return memberService.listMember();
	}
	
}
