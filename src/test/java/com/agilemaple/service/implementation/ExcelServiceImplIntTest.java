package com.agilemaple.service.implementation;

import java.util.List;


import junit.framework.Assert;

import org.aspectj.lang.annotation.Before;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.agilemaple.service.ExcelService;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:**/servlet-context.xml"})
public class ExcelServiceImplIntTest {
	
	@Autowired	
	private ExcelService excelService;
	
	@Test
	public void	fetchDataFromExcel(){
	    excelService.fetchDataFromExcel();
		
	}
}
