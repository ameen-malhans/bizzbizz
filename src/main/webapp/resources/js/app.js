var bizzbizzApp = angular.module("bizzbizzApp", ['ngRoute', 'ngAnimate', 'ngResource','angularUtils.directives.dirPagination']);

//Service ============================================
bizzbizzApp.service('memberService', function ($log, $resource) {
    this.getAllMembers =  function () {
        var memberResource = $resource('/api/rest/member', {}, {
            query: {method: 'GET', params: {}, isArray: true}
        });
        return memberResource.query();
    }; 
});

//ROUTING ===============================================
bizzbizzApp.config(function($routeProvider) {
console.log($routeProvider);	
 $routeProvider  
     .when('/', {
         templateUrl: 'resources/pages/member-ui.html',
         controller: 'memberController'
     })

});

//CONTROLLERS ============================================
bizzbizzApp.controller('memberController', function ($scope, $routeParams, $log, memberService) {
	$scope.members = memberService.getAllMembers();
	
});

