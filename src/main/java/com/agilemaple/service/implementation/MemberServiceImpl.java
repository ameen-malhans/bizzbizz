package com.agilemaple.service.implementation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.agilemaple.dao.MemberDAO;
import com.agilemaple.model.Member;
import com.agilemaple.service.MemberService;




@Component
public class MemberServiceImpl implements MemberService {
	
	@Autowired
	private MemberDAO MemberDAO;

/*	public void setPersonDAO(PersonDAO personDAO) {
		this.personDAO = personDAO;
	}*/

	@Override
	@Transactional
	public Integer addMember(Member c) {
		return this.MemberDAO.addMember(c);
	}

	@Override
	@Transactional
	public List<Member> listMember() {
		return this.MemberDAO.listMember();
	}



	
}
