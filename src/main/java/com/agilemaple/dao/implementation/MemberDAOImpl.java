package com.agilemaple.dao.implementation;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.agilemaple.dao.MemberDAO;
import com.agilemaple.model.Member;


@Repository
public class MemberDAOImpl implements MemberDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(MemberDAOImpl.class);
    @Autowired
	private SessionFactory sessionFactory; //session library of hibernate
	
	public void setSessionFactory(SessionFactory sf){
		this.sessionFactory = sf;
	}
    
	@Override
	public Integer addMember(Member c) {
		Session session = this.sessionFactory.getCurrentSession();
		Integer id = (Integer)session.save(c);
		logger.info("Member saved successfully, Member Details="+c);
		return id;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Member> listMember() {
		Session session = this.sessionFactory.getCurrentSession();
		List<Member> MemberList = session.createQuery("from Member").list();
		for(Member c : MemberList){
			logger.info("Member List::"+c);
		}
		return MemberList;
	}



}
