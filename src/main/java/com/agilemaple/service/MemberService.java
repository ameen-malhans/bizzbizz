package com.agilemaple.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.agilemaple.model.Member;


@Service
public interface MemberService {
	public Integer addMember(Member c);
	public List<Member> listMember();
	
}
