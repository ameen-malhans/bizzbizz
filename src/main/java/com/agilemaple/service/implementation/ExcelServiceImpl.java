package com.agilemaple.service.implementation;

import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.agilemaple.service.MemberService;
import com.agilemaple.model.Member;
import com.agilemaple.service.ExcelService;

import java.io.*;

@Component
public class ExcelServiceImpl implements ExcelService {

	
	@Autowired
	private MemberService memberService;
	
	private static String PATH_FOR_EXCEL_SHEET = "/home/ameen/dev/projects/bizzbizz/bizbizDataExcel.xls";

	@Override
	@Transactional
	public void fetchDataFromExcel() {
		try {

			FileInputStream file = new FileInputStream(
					new File(
							PATH_FOR_EXCEL_SHEET));

			// Get the workbook instance for XLS file
			XSSFWorkbook workbook = new XSSFWorkbook(file);

			// Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);

			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				Row row = sheet.getRow(i);
				Member member = new Member();
				member.setFirstName(row.getCell(1).toString());
				member.setLastName(row.getCell(2).toString());
				member.setBusinessName(row.getCell(3).toString());
				memberService.addMember(member);
				System.out.println("");

			}

			file.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
