package com.agilemaple.dao;

import java.util.List;

import org.springframework.stereotype.Service;

import com.agilemaple.model.Member;

@Service
public interface MemberDAO {

	public Integer addMember(Member c);
	public List<Member> listMember();
}
