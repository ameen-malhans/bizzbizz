<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page session="false"%>
<html ng-app="bizzbizzApp">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Home | E-Shop</title>
         <link href="<c:url value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" />
    
</head>
<body>
 
<div id="main">
    <div ng-view ></div>
</div>
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/angular/angular.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/angular/angular-resource.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/angular/angular-route.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/angular/angular-animate.js" />"></script>
	<script type="text/javascript" src="<c:url value="/resources/js/angular/dirPagination.js" />"></script>
	<script src="<c:url value="/resources/js/bootstrap.min.js" />"></script>
	<script src="<c:url value="/resources/js/jquery.scrollUp.min.js" />"></script>
	<script src="<c:url value="/resources/js/price-range.js" />"></script>
    <script src="<c:url value="/resources/js/jquery.prettyPhoto.js" />"></script>
    <script src="<c:url value="/resources/js/main.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/app.js" />"></script>
</body>
</html>
